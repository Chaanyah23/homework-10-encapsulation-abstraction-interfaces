public class BankAccount {

    private int balance = 0;
    private int transaction = 150;

    public void setBalance(int money) {
        balance = money - transaction;
        if (balance > 0) {
            System.out.println("Your balance has changed by $" + transaction + " and now is $" + balance);
        }
        else if (balance < 0) {
            System.out.println("Failed transaction");
        }
    }

    public int getBalance() {
        return balance;
    }

}